@extends('layout.master')

@section('judul')
  Buat Akun Baru
@endsection

@section('content')
<nav><a href="/">Home</a></nav>
<form action="/welcome" method="POST">
    @csrf
    <label for="">First Name :</label><br />
    <input type="text" name="first" required autocomplete="off"/><br /><br />
    <label for="">Last Name :</label><br />
    <input type="text" name="last" required autocomplete="off"/><br /><br />
    <label for="">Gender</label><br />
    <input type="radio" name="male"/>Male <br />
    <input type="radio" name="female"/>Female <br /><br />
    <label for="">Nationality</label><br />
    <select name="nationality" id="">
        <option value="indo">Indonesia</option>
        <option value="dll">dll</option>
    </select><br /><br />
    <label for="">Language Spoken</label><br />
    <input type="checkbox" name="indo"/>Bahasa Indonesia <br />
    <input type="checkbox" name="eng"/>English <br />
    <input type="checkbox" name="other"/>Other <br /><br />
    <label for="">Bio</label><br>
    <textarea name="bio" id="" cols="70" rows="5"></textarea><br /><br />
    <input type="submit" value="Sign Up"/>
</form>
@endsection
    