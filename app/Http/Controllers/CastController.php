<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    // function validasi & insert data cast ke tabel cast datatbase 
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'nama tidak boleh kosong!',
            'umur.required' => 'umur tidak boleh kosong!',
            'bio.required' => 'bio tidak boleh kosong!',
        ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'], 
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );
        
        return redirect('/cast');
    }

    // function menampilkan data cast
    public function index(){
        $casts = DB::table('cast')->get();
 
        return view('cast.index', \compact('casts'));
    }

    // function show detail cast
    public function show($id){
        $castshow = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', compact('castshow'));
    }

    // function ke halaman edit cast
    public function edit($id){
        $castedit = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('castedit'));
    }

    // function update data ke table cast
    public function update($id, Request $request){
        // validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'nama tidak boleh kosong!',
            'umur.required' => 'umur tidak boleh kosong!',
            'bio.required' => 'bio tidak boleh kosong!',
        ]
        );
        
        DB::table('cast')->where('id', $id)
              ->update(
                  [
                      'nama' => $request['nama'],
                      'umur' => $request['umur'],
                      'bio' => $request['bio'],
                  ]
                );
        return redirect('/cast');
    }

    // function delete data
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}