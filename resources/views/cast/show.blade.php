@extends('layout.master')

@section('judul')
 Halaman Detail Cast
@endsection

@section('content')

<h1>{{ $castshow->nama }}</h1>
<p>Umur : {{ $castshow->umur }}</p>
<p>Biodata : {{ $castshow->bio }}</p>

<a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection