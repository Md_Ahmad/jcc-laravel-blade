<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::get('/table', function () {
    return view('halaman.table');
});
Route::get('/data-tables', function () {
    return view('halaman.data-tables');
});



Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@daftar');

Route::post('/welcome', 'AuthController@welcome');

// CRUD Cast
// mengarahkan ke form create data
Route::get('/cast/create', 'CastController@create');
// menyimpan data ke table cast
Route::post('/cast', 'CastController@store');
// menampilkan semua data 
Route::get('/cast', 'CastController@index');
// detail data
Route::get('/cast/{cast_id}', 'CastController@show');
// mengarah ke form edit data
Route::get('cast/{cast_id}/edit', 'CastController@edit');
// update data ke table cast
Route::put('cast/{cast_id}', 'CastController@update');
// delete data
Route::delete('cast/{cast_id}', 'CastController@destroy');