@extends('layout.master')

@section('judul')
  Selamat Datang
@endsection

@section('content')
    <nav><a href="/">Home</a> |<a href="/register">Daftar</a></nav>
    <h1>SELAMAT DATANG! <font style="color: blue"> {{ $firstname }} {{ $lastname }}.. :)</font></h1>
    <h4>Terimakasih Telah Mendaftar di Website Kami. Media Belajar Kita Bersama!</h4>
@endsection
    