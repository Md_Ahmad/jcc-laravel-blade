@extends('layout.master')

@section('judul')
 Halaman Data Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Biodata</th>
            <th scope="col">action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td>{{$item->bio}}</td>
                    <td>
                        <form action="cast/{{ $item->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                        </form>
                    </td>
                </tr>
            @empty
               <h1>data kosong</h1>
            @endforelse
        </tbody>
    </table>
@endsection